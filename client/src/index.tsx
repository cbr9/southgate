import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App/App';
import * as serviceWorker from './serviceWorker';
import { ApolloClient, ApolloProvider, HttpLink, InMemoryCache } from '@apollo/client';
import { TrackDetails } from './components/Track';

export const urlFromBackend = (url: string) => {
	const server = `https://localhost:${process.env.REACT_APP_API}`;
	return `${server}${url}`;
};

const client = new ApolloClient({
	link: new HttpLink({
		uri: urlFromBackend('/query'),
		credentials: 'include',
	}),
	cache: new InMemoryCache({
		typePolicies: {
			Track: {
				fields: {
					details: {
						merge(existing: TrackDetails, incoming: TrackDetails, { mergeObjects }): TrackDetails {
							return mergeObjects(existing, incoming);
						},
					},
				},
			},
		},
	}),
	connectToDevTools: true,
});

const app = (
	<ApolloProvider client={client}>
		<App />
	</ApolloProvider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
