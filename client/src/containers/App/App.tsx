import React, { Suspense } from 'react';
import { GlobalStyle, Main, Spinner, theme } from '../../theme/theme';
import styled, { ThemeProvider } from 'styled-components';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';

//////////////////////////////////////
// REACT COMPONENTS ///////////////////
//////////////////////////////////////

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	& > * {
		max-width: 100vw;
	}
`;

const Index = React.lazy(() => import('../../components/Index'));
const Contact = React.lazy(() => import('../../components/Contact'));
const Biography = React.lazy(() => import('../../components/Biography'));
const Discography = React.lazy(() => import('../Discography/Discography'));
const Gallery = React.lazy(() => import('../../components/Gallery'));
const Login = React.lazy(() => import('../../components/Login'));

export interface AppProps {
	isLoggedIn: boolean;
	setIsLoggedIn: (value: React.SetStateAction<boolean>) => void;
}

const App = () => {
	return (
		<ThemeProvider theme={theme}>
			<Wrapper className="App">
				<GlobalStyle />
				<BrowserRouter>
					<NavBar />
					<Main>
						<Suspense fallback={Spinner}>
							<Switch>
								<Route path="/" exact component={Index} />
								<Route path="/contacto" exact component={Contact} />
								<Route path="/bio" exact component={Biography} />
								<Route path="/discografia" exact component={Discography} />
								<Route path="/galeria" exact component={Gallery} />
								<Route path="/login" exact component={Login} />
								<Route component={() => <p>404</p>} />
							</Switch>
						</Suspense>
					</Main>
					<Footer />
				</BrowserRouter>
			</Wrapper>
		</ThemeProvider>
	);
};

export default React.memo(App);
