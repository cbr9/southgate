import * as React from 'react';
import { useCallback, useState } from 'react';
import Record, { Details as RecordDetails } from '../../components/Record';
import { gql, useQuery } from '@apollo/client';
import { Spinner, ThemeValues } from '../../theme/theme';
import styled from 'styled-components';

//////////////////////////////////////////////
// INTERFACES & TYPES ////////////////////////
//////////////////////////////////////////////

export interface Context {
	currentRecord: number | undefined;
	updateRecord: (index: number | undefined) => void;
}

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const Error = styled.span`
	background: rgba(0, 0, 0, 0.5);
	box-shadow: ${(props: ThemeValues) => props.theme.bigShadow};
	display: inline-block;
	width: max-content;
	padding: 2vh 2vw;
	align-self: center;
`;

//////////////////////////////////////
// GRAPHQL QUERIES & TYPES ///////////
//////////////////////////////////////

export const RETRIEVE_DISCOGRAPHY = gql`
	query {
		records {
			id
			title
			image
			year
			tracks {
				title
			}
		}
	}
`;

export interface Response {
	records: RecordDetails[];
}

//////////////////////////////////////
// REACT COMPONENT ///////////////////
//////////////////////////////////////

export const DiscographyContext = React.createContext<Context | undefined>(undefined);

const Discography = () => {
	const { error, data } = useQuery<Response, null>(RETRIEVE_DISCOGRAPHY);
	const [currentRecord, setCurrentRecord] = useState<number | undefined>(undefined);
	const updateRecord = useCallback((index: number | undefined) => {
		setCurrentRecord(index);
	}, []);

	if (error) {
		return <Error>Ha habido un error en el servidor. Prueba a recargar la pagina</Error>;
	}
	return (
		<>
			{<Spinner show={!data} />}
			{data?.records &&
				data.records.length > 0 &&
				data.records.map((record: RecordDetails, index: number) => {
					return (
						<DiscographyContext.Provider value={{ currentRecord, updateRecord }}>
							<Record info={record} key={record.id} index={index} />
						</DiscographyContext.Provider>
					);
				})}
		</>
	);
};

// EXPORT
export default Discography;
