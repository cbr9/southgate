import React from 'react';
import styled, { createGlobalStyle, keyframes } from 'styled-components';
import background from '../assets/img/index/fondo-min.jpg';
import { Roller } from 'react-awesome-spinners';
import { CSSTransition } from 'react-transition-group';

//////////////////////////////////////
// CSS ANIMATIONS ////////////////////
//////////////////////////////////////

export const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

export const popup = keyframes`
	0% {
		opacity: 0;
		transform: scale(-100%);
	}
	100% {
		opacity: 1;	
		transform: scale(100%);
	}
`;

//////////////////////////////////////
// GLOBAL STYLE //////////////////////
//////////////////////////////////////

export const GlobalStyle = createGlobalStyle`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
    font-size: inherit;
	font-family: inherit;
	font-weight: inherit;
	text-align: inherit;
    margin: 0;
  	padding: 0;
  }
  
  html {
    scroll-behavior: smooth;
    overflow-x: hidden;
    &::-webkit-scrollbar {
    display: none;
    }
  }
  
  body {
    font-family: 'Source Sans Pro', sans-serif;
    font-size: calc(1em + .5vw);
    font-weight: 400;
    line-height: 1.7;
    color: white;
    text-align: center;
    display: flex;
    flex-direction: column;
  }

  a:link,
  a:visited {
    text-decoration: none;
    color: inherit;
  }

  svg {
    fill: currentColor;
  }
`;

//////////////////////////////////////
// THEME VARIABLES ///////////////////
//////////////////////////////////////

export const theme = {
	colorBgIndexLight: '#333',
	colorBgIndexMedium: '#222',
	colorBgIndexDark: '#111',
	colorText: 'white',
	baseShadow: '0 0 1rem black',
	bigShadow: '0 0 2rem black',
	contactShadow: '0 -1rem 5rem black',
	navBarHeight: '10',
	smallPhone: '500px',
	phone: '600px',
	tabletPortrait: '900px',
	laptop: '1024px',
	tabletLandscape: '1200px',
	laptopLarge: '1440px',
	bigDesktop: '1800px',
	hiDpi: '2560px', // 4K
	lpWidth: '80', // vh or vw
	lpHeight: '80vh',
	cardAnimationTime: '200ms',
};

export interface ThemeValues {
	theme: { [K in keyof typeof theme]+?: typeof theme[K] };
}

//////////////////////////////////////////////
// GLOBAL STYLED-COMPONENTS //////////////////
//////////////////////////////////////////////

export const Main = styled.main`
	&:before {
		content: ' ';
		position: fixed; // instead of background-attachment
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		background: linear-gradient(to top left, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(${background}) no-repeat center center;
		background-size: cover;
		will-change: transform; // creates a new paint layer
		z-index: -1;
	}
	padding: 2vh 2vw;
	display: flex;
	flex-direction: column;
	justify-content: center;
	margin-top: ${(props: ThemeValues) => props.theme.navBarHeight}vh;
	min-height: ${(props: ThemeValues) => 100 - parseInt(props.theme.navBarHeight as string)}vh;
`;

export const Card = styled.div`
	box-shadow: ${(props: ThemeValues) => props.theme.bigShadow};
	background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	position: relative;
	animation: ${fadeIn} ${(props: ThemeValues) => props.theme.cardAnimationTime} ease-in-out;
	align-self: center;
`;

const SpinnerWrapper = styled.div`
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	display: flex;
	justify-content: center;
	align-items: center;
	z-index: 100;

	&.fade-enter {
		opacity: 0;
	}
	&.fade-enter-active {
		opacity: 1;
		transition: opacity 300ms;
	}
	&.fade-exit {
		opacity: 1;
	}
	&.fade-exit-active {
		opacity: 0;
		transition: opacity 300ms;
	}
`;

export const Spinner = (props: { show: boolean }) => {
	return (
		<CSSTransition timeout={300} in={props.show} classNames={'fade'} mountOnEnter unmountOnExit>
			<SpinnerWrapper>
				<Roller color="white" />
			</SpinnerWrapper>
		</CSSTransition>
	);
};
