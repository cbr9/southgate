import * as React from 'react';
import { useCallback, useRef, useState } from 'react';
import styled from 'styled-components';
import { fadeIn, ThemeValues } from '../theme/theme';
import { gql, useMutation } from '@apollo/client';

//////////////////////////////////////
// INTERFACES & TYPES ////////////////
//////////////////////////////////////

export interface InputState {
	value: string;
	isValid: boolean;
}

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const FormContainer = styled.form`
	width: 70vw;
	align-self: center;
	background-color: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	box-shadow: ${(props: ThemeValues) => props.theme.contactShadow};
	animation: ${fadeIn} ${(props: ThemeValues) => props.theme.cardAnimationTime} ease-in-out;
`;

const InputWrapper = styled.div`
	padding: 2rem;
`;

const Input = styled.div`
	&:not(:last-of-type) {
		margin-bottom: 0.5rem;
	}
	display: flex;
	margin: 0 auto;
	flex-direction: column;
	font-size: 85%;
	& p {
		width: max-content;
		margin-left: 1rem;
		margin-top: 1rem;
	}
`;

const InputField = styled.input`
	margin-left: 0.5rem;
	background: ${(props: ThemeValues & Partial<Pick<InputState, 'isValid'>>) => props.theme.colorBgIndexMedium};
	border: none;
	box-shadow: ${(props: ThemeValues & Partial<Pick<InputState, 'isValid'>>) => props.theme.baseShadow};
	height: 4rem;
	color: white;
	text-align: center;
	&:focus {
		outline: ${(props: ThemeValues & Partial<Pick<InputState, 'isValid'>>) => (!props.isValid ? '1px solid red' : 'none')};
	}
`;

const Message = styled.textarea`
	margin-left: 0.5rem;
	background: ${(props: ThemeValues & Partial<Pick<InputState, 'isValid'>>) => props.theme.colorBgIndexMedium};
	border: none;
	box-shadow: ${(props: ThemeValues & Partial<Pick<InputState, 'isValid'>>) => props.theme.baseShadow};
	color: white;
	overflow: hidden;
	height: min-content;
	outline: none;
	resize: none;
	text-align: center;
	padding: 0.5rem;
	&:focus {
		outline: ${(props: ThemeValues & Partial<Pick<InputState, 'isValid'>>) => (!props.isValid ? '1px solid red' : 'none')};
	}
`;

const Submit = styled(InputField)`
	margin-top: 1rem;
	padding: 1rem;
	font-family: inherit;
	outline: none;
	transition: all 0.1s;
	&:focus {
		outline: none;
	}
	&:active {
		transform: ${(props: ThemeValues & Partial<Pick<InputState, 'isValid'>>) => (props.isValid ? 'translateY(1px)' : 'none')};
	}
`;

//////////////////////////////////////
// GRAPHQL QUERIES & TYPES ///////////
//////////////////////////////////////

const SEND_MESSAGE = gql`
	mutation SendMessage($input: MessageInput!) {
		sendMessage(input: $input) {
			body
			email
			name
		}
	}
`;

interface ContactMessage {
	name: string;
	email: string;
	body: string;
}

interface Response {
	sendMessage: ContactMessage;
}

interface MutationParams {
	input: ContactMessage;
}

export const validateEmail = (email: string): boolean => {
	const regex = new RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);
	return regex.test(email);
};

//////////////////////////////////////
// REACT COMPONENT ///////////////////
//////////////////////////////////////

const Contact = () => {
	const [email, setEmail] = useState<InputState>({
		value: '',
		isValid: false,
	});
	const emailRef = useRef<HTMLInputElement>(null);
	const [name, setName] = useState<InputState>({
		value: '',
		isValid: false,
	});
	const nameRef = useRef<HTMLInputElement>(null);
	const [body, setBody] = useState<InputState>({
		value: '',
		isValid: false,
	});
	const bodyRef = useRef<HTMLTextAreaElement>(null);
	const [sendContactMessage] = useMutation<Response, MutationParams>(SEND_MESSAGE, {
		onCompleted: () => {
			setIsSent(true);
		},
	});
	const [isSent, setIsSent] = useState(false);

	const onChangeHandler = useCallback(
		(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, type: 'name' | 'email' | 'body'): void => {
			switch (type) {
				case 'name':
					setName({ value: event.target.value, isValid: event.target.value !== '' });
					break;
				case 'email':
					setEmail({ value: event.target.value, isValid: validateEmail(event.target.value) });
					break;
				case 'body':
					setBody({ value: event.target.value, isValid: event.target.value !== '' });
					break;
			}
			if (isSent) {
				setIsSent(false);
			}
		},
		[validateEmail],
	);

	const onSubmit = useCallback(
		(event: React.FormEvent<HTMLFormElement>, data: { name: InputState; email: InputState; body: InputState }): void => {
			event.preventDefault();
			if (data.name.isValid && data.email.isValid && data.body.isValid) {
				sendContactMessage({
					variables: {
						input: {
							name: data.name.value,
							email: data.email.value,
							body: data.body.value,
						},
					},
				})
					.then(() => {
						setName({ value: '', isValid: false });
						setEmail({ value: '', isValid: false });
						setBody({ value: '', isValid: false });
					})
					.catch(() => {});
			} else {
				if (!data.name.isValid) {
					nameRef.current?.focus();
				} else if (!data.email.isValid) {
					emailRef.current?.focus();
				} else if (!data.body.isValid) {
					bodyRef.current?.focus();
				}
			}
		},
		[sendContactMessage],
	);

	return (
		<>
			<FormContainer onSubmit={(event: React.FormEvent<HTMLFormElement>) => onSubmit(event, { name, email, body })}>
				<InputWrapper>
					<Input>
						<label>Nombre y apellidos:</label>
						<InputField
							ref={nameRef}
							type="text"
							name="name"
							autoComplete="off"
							isValid={name.isValid}
							value={name.value}
							onChange={(event: React.ChangeEvent<HTMLInputElement>) => onChangeHandler(event, 'name')}
						/>
					</Input>
					<Input>
						<label>Email:</label>
						<InputField
							ref={emailRef}
							type="email"
							name="email"
							autoComplete="off"
							isValid={email.isValid}
							value={email.value}
							onChange={(event: React.ChangeEvent<HTMLInputElement>) => onChangeHandler(event, 'email')}
						/>
					</Input>
					<Input>
						<label>Mensaje:</label>
						<Message
							ref={bodyRef}
							name="message"
							rows={10}
							isValid={body.isValid}
							value={body.value}
							onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => onChangeHandler(event, 'body')}
						/>
					</Input>
					<Submit type="submit" value="Enviar" />
				</InputWrapper>
			</FormContainer>
		</>
	);
};

export default React.memo(Contact);
