import React from 'react';
import styled from 'styled-components';
import { fadeIn, ThemeValues } from '../theme/theme';
import alberto from '../assets/img/gallery/alberto-min.jpg';
import group from '../assets/img/gallery/group-min.jpg';
import luismi from '../assets/img/gallery/luismi-min.jpg';
import jp from '../assets/img/gallery/jp-min.jpg';
import diego from '../assets/img/gallery/diego-min.jpg';
import pablo from '../assets/img/gallery/pablo-min.jpg';

//////////////////////////////////////
// INTERFACES & TYPES ////////////////
//////////////////////////////////////

interface Props {
	src: string;
	name?: string;
}

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const Wrapper = styled.div`
	display: grid;
	grid-template-rows: repeat(3, max-content);
	grid-template-columns: repeat(3, 1fr);
	grid-auto-rows: max-content;
	grid-gap: 1rem;
	@media only screen and (max-width: 900px) {
		grid-template-columns: repeat(2, 1fr);
	}
	@media only screen and (max-width: 600px) {
		grid-template-columns: 1fr;
	}
`;

const MusicianWrapper = styled.div`
	display: flex;
	flex-direction: column;
	animation: ${fadeIn} ${(props: ThemeValues) => props.theme.cardAnimationTime} ease-in-out;
	span {
		display: block;
		box-shadow: ${(props: ThemeValues) => props.theme.baseShadow};
		background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	}
	img {
		width: 100%;
		height: auto;
	}

	&:last-of-type {
		@media only screen and (max-width: 900px) {
			grid-column: 1 / span 2;
			width: 50%;
			justify-self: center;
		}
		@media only screen and (max-width: 600px) {
			grid-column: 1 / span 1;
			width: 100%;
		}
	}
`;

const GroupWrapper = styled(MusicianWrapper)`
	grid-row: 1 / span 2;
	grid-column: 1 / span 2;
	height: 100%;
	span {
		height: 100%;
		display: flex;
		flex-direction: column;
		justify-content: center;
	}
	@media only screen and (max-width: 600px) {
		grid-column: 1 / span 1;
		grid-row: 1 / span 1;
	}
`;

const Header = styled.h2`
	width: 100%;
	align-self: center;
	box-shadow: ${(props: ThemeValues) => props.theme.baseShadow};
	background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	margin-bottom: 1rem;
	&:not(:first-of-type) {
		margin-top: 3rem;
		border-top: 1px solid white;
	}
`;

//////////////////////////////////////
// REACT COMPONENTS //////////////////
//////////////////////////////////////

const routes = [alberto, diego, pablo, jp, luismi];
const musicians = [
	'Alberto Gil Hernandez',
	'Diego Chozas Cano',
	'Pablo Yañez Rodriguez',
	'Juan Pablo Herrera Castillo',
	'Luis Miguel Rojas Mori',
];

const Musician = (props: Props) => {
	return (
		<MusicianWrapper>
			<img src={props.src} alt={props.name} />
			<span>{props.name}</span>
		</MusicianWrapper>
	);
};

const Group = (props: Props) => {
	return (
		<GroupWrapper>
			<img src={props.src} alt={props.name} />
			<span>{props.name}</span>
		</GroupWrapper>
	);
};

const Gallery = () => {
	return (
		<>
			<Header>Fotos</Header>
			<Wrapper>
				<Group src={group} name="SouthGate" />
				{React.Children.toArray(
					musicians.map((key: string, i: number) => {
						return <Musician src={routes[i]} name={key} />;
					}),
				)}
			</Wrapper>
			<Header>Videos</Header>
		</>
	);
};

//////////////////////////////////////
// REDUX MAPS ////////////////////////
//////////////////////////////////////

// EXPORT
export default Gallery;
