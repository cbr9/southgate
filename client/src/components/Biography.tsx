import styled from 'styled-components';
import { Card, Spinner, ThemeValues } from '../theme/theme';
import React from 'react';
import { gql, useQuery } from '@apollo/client';
import { urlFromBackend } from '../index';

//////////////////////////////////////
// INTERFACES ////////////////////////
//////////////////////////////////////

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const Wrapper = styled(Card)`
	display: flex;
	padding: 3rem;
	@media (max-width: ${(props: ThemeValues) => props.theme.tabletLandscape}) {
		flex-direction: column;
	}
`;

const PosterWrapper = styled.div`
	padding-right: 2rem;
	border-right: 1px solid white;
	display: flex;
	flex-direction: column;
	justify-content: center;
	@media (max-width: ${(props: ThemeValues) => props.theme.tabletLandscape}) {
		border-right: none;
		padding-right: 0;
		border-top: 1px solid white;
		padding-top: 2rem;
	}
`;

const Poster = styled.img`
	float: left;
	box-shadow: ${(props: ThemeValues) => props.theme.bigShadow};
	height: max-content;
	width: max-content;
	@media (max-width: ${(props: ThemeValues) => props.theme.tabletLandscape}) {
		float: none;
		margin: 0 auto;
	}
`;

const ParagraphWrapperContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	font-size: 80%;
	@media (max-width: ${(props: ThemeValues) => props.theme.tabletLandscape}) {
		order: -1;
	}
`;

const ParagraphWrapper = styled.div`
	padding-left: 1rem;
	text-align: left;
	@media (max-width: ${(props: ThemeValues) => props.theme.tabletLandscape}) {
		padding-left: 0;
		padding-bottom: 1rem;
		text-align: center;
	}
`;

const Paragraph = styled.p``;

//////////////////////////////////////
// GRAPHQL QUERIES & TYPES ///////////
//////////////////////////////////////

const Query = gql`
	query {
		biography {
			image
			content
		}
	}
`;

interface Response {
	biography: {
		image: string;
		content: string;
	};
}

//////////////////////////////////////
// REACT COMPONENTS ///////////////////
//////////////////////////////////////

const Biography = () => {
	const { data, loading, error } = useQuery<Response>(Query);
	if (data) {
		return (
			<Wrapper>
				<PosterWrapper>
					<Poster src={urlFromBackend(data?.biography.image)} alt="cartel" />
				</PosterWrapper>
				<ParagraphWrapperContainer>
					<ParagraphWrapper>
						{React.Children.toArray(
							data?.biography.content.split('\n\n').map(block => {
								return (
									<>
										<Paragraph>{block}</Paragraph>
										<wbr />
									</>
								);
							}),
						)}
					</ParagraphWrapper>
				</ParagraphWrapperContainer>
			</Wrapper>
		);
	} else if (error) {
		return <p>404</p>;
	} else if (loading) {
		return <Spinner show={loading} />;
	} else {
		return <p>Ha ocurrido un error inesperado. Prueba a recargar la pagina</p>;
	}
};

export default React.memo(Biography);
