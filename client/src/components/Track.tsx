import * as React from 'react';
import { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import { ThemeValues } from '../theme/theme';
import { gql, useApolloClient, useLazyQuery } from '@apollo/client';
import { Context as RecordProps, RecordContext } from './Record';
import { Context as DiscographyProps, DiscographyContext } from '../containers/Discography/Discography';

//////////////////////////////////////////////
// INTERFACES & TYPES ////////////////////////
//////////////////////////////////////////////

// type DispatchProps = Pick<Dispatchers, 'updateTrack' | 'toggleButtons'>;
interface Props {
	title: string;
	index: number; // track index
	record: number; // record index
}

export interface TrackInfo {
	title: string;
	details: {
		lyrics?: string;
		audio?: string;
	};
}

export type TrackDetails = Omit<TrackInfo, 'title'>;

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const TrackItem = styled.li`
	list-style: none;
	padding: 0.2rem 1rem;
	background: ${(props: ThemeValues & { isSelected: boolean }) => (props.isSelected ? 'white' : 'none')};
	color: ${(props: ThemeValues & { isSelected: boolean }) => (props.isSelected ? 'black' : 'none')};
	transition: all 0.3s;
	& button {
		background: none;
		border: none;
		cursor: pointer;
		color: inherit;
		&:hover {
			border-bottom: 1px solid white;
		}
	}
`;

/////////////////////////////////////////////////
// GRAPHQL QUERIES  /////////////////////////////
/////////////////////////////////////////////////

export const GET_AUDIO_URL = gql`
	query getTrackDetails($input: TrackIndicesInput!) {
		track(input: $input) {
			details {
				audio
			}
		}
	}
`;

export interface QueryParams {
	input: {
		track: number;
		record: number;
	};
}

export interface Response {
	track: {
		details: {
			audio: string;
		};
	};
}

//////////////////////////////////////
// REACT COMPONENTS ///////////////////
//////////////////////////////////////

const Track = (props: Props) => {
	const [selected, setSelected] = useState(false);
	const client = useApolloClient();
	const recordContext = useContext(RecordContext);
	const discographyContext = useContext(DiscographyContext);
	const { currentRecord, updateRecord } = discographyContext as DiscographyProps;
	const { currentTrack, updateTrack, blockedBtn } = recordContext as RecordProps;

	const [getAudio] = useLazyQuery<Response, QueryParams>(GET_AUDIO_URL, {
		variables: {
			input: {
				track: props.index,
				record: props.record,
			},
		},
		onCompleted: () => {
			updateTrack(props.index);
			updateRecord(props.record);
			setSelected(true);
		},
		onError: error => {
			console.log(error);
		},
	});

	useEffect(() => {
		if (currentTrack === props.index && currentRecord === props.record) {
			setSelected(true);
		} else {
			if (selected) {
				setSelected(false);
			}
		}
	}, [currentTrack, currentRecord, getAudio, props.index, props.record, selected]);

	return (
		<TrackItem isSelected={selected}>
			<button
				disabled={blockedBtn}
				className="outlined"
				onClick={() => {
					try {
						client.readQuery<Response, QueryParams>({
							query: GET_AUDIO_URL,
							variables: {
								input: {
									track: props.index,
									record: props.record,
								},
							},
						});

						// updates should be batched
						updateTrack(props.index);
						updateRecord(props.record);
						setSelected(true);
					} catch (e) {
						getAudio();
					}
				}}
			>
				{props.title}
			</button>
		</TrackItem>
	);
};

// EXPORT
export default Track;
