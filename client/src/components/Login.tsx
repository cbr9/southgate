import React, { ChangeEvent, FormEvent, useState } from 'react';
import cookies from 'react-cookies';
import { gql, useMutation } from '@apollo/client';
import { InputState, validateEmail } from './Contact';
import { useHistory } from 'react-router';

//////////////////////////////////////
// GRAPHQL QUERIES & TYPES ///////////
//////////////////////////////////////

interface Response {
	login: {
		authToken: {
			accessToken: string;
			expiredAt: Date;
		};
	};
}

interface LoginParams {
	input: {
		email: string;
		password: string;
	};
}

const loginUser = gql`
	mutation Login($input: LoginInput!) {
		login(input: $input) {
			authToken {
				accessToken
				expiredAt
			}
		}
	}
`;

//////////////////////////////////////
// REACT COMPONENT ///////////////////
//////////////////////////////////////

const Login = () => {
	const history = useHistory();
	const [email, setEmail] = useState<InputState>({
		value: '',
		isValid: false,
	});
	const [pwd, setPwd] = useState<Pick<InputState, 'value'>>({
		value: '',
	});
	const [login] = useMutation<Response, LoginParams>(loginUser);
	const onSubmit = (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		if (email.isValid) {
			login({
				variables: {
					input: {
						email: email.value,
						password: pwd.value,
					},
				},
			}).then(() => {
				console.log(cookies.load('token'));
				history.push('/');
			});
		}
	};
	return (
		<form onSubmit={onSubmit}>
			<label htmlFor="">Correo electronico</label>
			<input
				type="email"
				onChange={(event: ChangeEvent<HTMLInputElement>) =>
					setEmail({ value: event.target.value, isValid: validateEmail(event.target.value) })
				}
			/>
			<label htmlFor="">Contraseña</label>
			<input
				type="password"
				value={pwd.value}
				onChange={(event: ChangeEvent<HTMLInputElement>) => setPwd({ value: event.target.value })}
			/>
			<input type="submit" />
		</form>
	);
};

export default Login;
