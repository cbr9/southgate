import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import AwesomeSlider from 'react-awesome-slider';
import styled from 'styled-components';
import { ThemeValues } from '../theme/theme';
import 'react-awesome-slider/dist/styles.css';
import { CSSTransition } from 'react-transition-group';
import { GET_AUDIO_URL, QueryParams, Response } from './Track';
import { urlFromBackend } from '../index';
import { ApolloError, useApolloClient, useLazyQuery } from '@apollo/client';
import {
	Context as DiscographyProps,
	DiscographyContext,
	Response as DiscographyResponse,
	RETRIEVE_DISCOGRAPHY,
} from '../containers/Discography/Discography';
import { ReactComponent as Back } from '../assets/svg/controller-back.svg';
import { ReactComponent as Next } from '../assets/svg/controller-next.svg';
import { ReactComponent as Stop } from '../assets/svg/controller-stop.svg';
import { ReactComponent as Play } from '../assets/svg/controller-play.svg';
import { ReactComponent as Pause } from '../assets/svg/controller-pause.svg';
import { Context as RecordProps, RecordContext } from './Record';

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const Player = styled.div`
	background: rgba(0, 0, 0, 0.7);
	display: grid;
	grid-template-rows: 50% 25% 25%;
	grid-template-columns: repeat(6, 1fr);
	grid-template-areas:
		'display  display  display  display  display  display'
		'stop     stop     stop     play     play     play'
		'back     back     pause    pause    next     next';
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;

	&.fade-enter {
		opacity: 0;
	}
	&.fade-enter-active {
		opacity: 1;
		transition: opacity 300ms;
	}
	&.fade-exit {
		opacity: 1;
	}
	&.fade-exit-active {
		opacity: 0;
		transition: opacity 300ms;
	}
`;

const Btn = styled.button`
	width: 100%;
	height: 100%;
	border: none;
	outline: none;
	box-shadow: ${(props: ThemeValues) => props.theme.bigShadow};
	background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	cursor: pointer;
	display: inline-block;
	transition: all 0.2s cubic-bezier(0.46, 0.03, 0.52, 0.96);
	& svg {
		width: 100%;
		height: 100%;
		color: white;
	}
`;

const UpperRowBtn = styled(Btn)`
	z-index: 7;
	&:active {
		transform: translateY(0.2rem);
	}
	&.play {
		grid-area: play;
	}
	&.stop {
		grid-area: stop;
	}
`;

const LowerRowBtn = styled(Btn)`
	z-index: 5;
	&:active {
		transform: translateY(-0.2rem);
	}
	&.back {
		grid-area: back;
	}
	&.pause {
		grid-area: pause;
	}
	&.next {
		grid-area: next;
	}
`;

const Carousel = styled(AwesomeSlider)`
	font-size: 200%;
	display: flex;
	justify-content: center;
	align-items: center;
	z-index: 0;
	grid-area: display;
	--slider-height-percentage: 0% !important;
	--content-background-color: none !important;
	--slider-transition-duration: 500ms;
	.awssld__controls {
		display: none;
	}
`;

const CarouselCover = styled.div`
	grid-area: display;
	background: transparent;
	z-index: 1;
`;

//////////////////////////////////////////////
// INTERFACES & TYPES ////////////////////////
//////////////////////////////////////////////

interface Props {
	show: boolean;
	index: number;
}

interface ControllerProps {
	onClick?: () => void;
	position: 'top' | 'bottom';
	name: string;
	disabled: boolean;
}

//////////////////////////////////////
// REACT COMPONENT ///////////////////
//////////////////////////////////////

const Controller = (props: ControllerProps) => {
	let svg: JSX.Element;
	switch (props.name) {
		case 'stop':
			svg = <Stop />;
			break;
		case 'play':
			svg = <Play />;
			break;
		case 'next':
			svg = <Next />;
			break;
		case 'back':
			svg = <Back />;
			break;
		case 'pause':
			svg = <Pause />;
			break;
		default:
			throw new Error('wrong svg name');
	}

	if (props.position === 'top') {
		return (
			<UpperRowBtn disabled={props.disabled} className={props.name} onClick={props.onClick}>
				{svg}
			</UpperRowBtn>
		);
	} else {
		return (
			<LowerRowBtn disabled={props.disabled} className={props.name} onClick={props.onClick}>
				{svg}
			</LowerRowBtn>
		);
	}
};

const AudioPlayer = (props: Props) => {
	const recordContext = useContext(RecordContext);
	const discographyContext = useContext(DiscographyContext);
	const { currentTrack, updateTrack, blockedBtn, toggleButtons } = recordContext as RecordProps;
	const { currentRecord, updateRecord } = discographyContext as DiscographyProps;

	const audio = useRef<HTMLAudioElement>(new Audio());
	const client = useApolloClient();
	const [src, setSrc] = useState('');

	const [getAudio] = useLazyQuery<Response, QueryParams>(GET_AUDIO_URL, {
		onCompleted: (data: Response) => {
			setSrc(urlFromBackend(data.track.details.audio ?? ''));
			if (currentTrack != null) {
				updateTrack(currentTrack + 1);
			}
		},
		onError: (error: ApolloError) => {
			console.log(error);
		},
	});

	useEffect(() => {
		if (currentRecord != null && currentTrack != null && currentRecord === props.index) {
			try {
				const newSrc = urlFromBackend(
					client.readQuery<Response, QueryParams>({
						query: GET_AUDIO_URL,
						variables: {
							input: {
								track: currentTrack,
								record: currentRecord,
							},
						},
					})?.track.details.audio ?? '',
				);
				if (newSrc !== src) {
					setSrc(newSrc);
				}
			} catch (error) {
				console.log(error.message);
			}
		}
	}, [currentTrack, currentRecord, props.index, client, src]);

	const nextOrBackHelper = useCallback(
		(variables: QueryParams) => {
			try {
				setSrc(
					urlFromBackend(
						client.readQuery<Response, QueryParams>({
							query: GET_AUDIO_URL,
							variables,
						})?.track.details.audio ?? '',
					),
				);
				updateTrack(variables.input.track);
			} catch (e) {
				getAudio({
					variables,
				});
			}
		},
		[updateTrack, client, getAudio],
	);

	const nextOrBack = useCallback(
		(action: 'next' | 'back'): void => {
			const num_tracks = client.readQuery<DiscographyResponse, null>({ query: RETRIEVE_DISCOGRAPHY })?.records[props.index].tracks
				.length;
			if (currentTrack != null && num_tracks != null) {
				if (action === 'next' && currentTrack < num_tracks - 1) {
					const variables: QueryParams = {
						input: {
							track: (currentTrack as number) + 1,
							record: currentRecord as number,
						},
					};
					nextOrBackHelper(variables);
				} else if (action === 'back' && currentTrack > 0) {
					const variables: QueryParams = {
						input: {
							track: (currentTrack as number) - 1,
							record: currentRecord as number,
						},
					};
					nextOrBackHelper(variables);
				}
			}
		},
		[currentTrack, client, props.index, nextOrBackHelper],
	);

	const play = useCallback(() => {
		audio.current.play();
	}, []);

	const pause = useCallback(() => {
		audio.current.pause();
	}, []);

	const stop = useCallback(() => {
		audio.current?.pause();
		updateRecord(undefined);
	}, [updateRecord]);

	return (
		<CSSTransition timeout={300} in={props.show} classNames={'fade'} mountOnEnter unmountOnExit>
			<Player>
				{currentTrack != null && <audio ref={audio} src={src} onLoadedMetadata={() => audio.current.play()}></audio>}
				<Carousel
					infinite={false}
					selected={currentTrack}
					organicArrows={false}
					bullets={false}
					onTransitionStart={() => toggleButtons()} // block
					onTransitionEnd={() => toggleButtons()} // unblock
				>
					{React.Children.toArray(
						// at this point the RETRIEVE_DISCOGRAPHY query should already be fetched, so no error should be thrown
						client
							.readQuery<DiscographyResponse, null>({ query: RETRIEVE_DISCOGRAPHY })
							?.records[props.index].tracks.map((track, index) => {
								return <div>{track.title}</div>;
							}),
					)}
				</Carousel>
				<CarouselCover />
				<Controller disabled={blockedBtn} position="top" name="play" onClick={play} />
				<Controller disabled={blockedBtn} position="top" name="stop" onClick={stop} />
				<Controller disabled={blockedBtn} position="bottom" name="back" onClick={() => nextOrBack('back')} />
				<Controller disabled={blockedBtn} position="bottom" name="pause" onClick={pause} />
				<Controller disabled={blockedBtn} position="bottom" name="next" onClick={() => nextOrBack('next')} />
			</Player>
		</CSSTransition>
	);
};

// EXPORT
export default React.memo(AudioPlayer);
