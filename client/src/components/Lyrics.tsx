import React, { useContext, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { CSSTransition } from 'react-transition-group';
import { ThemeValues } from '../theme/theme';
import { gql, useApolloClient, useLazyQuery } from '@apollo/client';
import { QueryParams } from './Track';
import { RecordContext } from './Record';
import { DiscographyContext } from '../containers/Discography/Discography';

//////////////////////////////////////////////
// INTERFACES & TYPES ////////////////////////
//////////////////////////////////////////////

interface Props {
	show: boolean;
}

//////////////////////////////////////////////
// STYLED COMPONENTS /////////////////////////
//////////////////////////////////////////////

const LyricsWrapper = styled.div`
	text-align: center;
	overflow-x: hidden;
	overflow-y: scroll;
	font-size: 90%;
	z-index: 1;
	background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;

	&::-webkit-scrollbar {
		display: none;
	}

	&.fade-enter {
		opacity: 0;
	}
	&.fade-enter-active {
		opacity: 1;
		transition: opacity 300ms;
	}
	&.fade-exit {
		opacity: 1;
	}
	&.fade-exit-active {
		opacity: 0;
		transition: opacity 300ms;
	}

	p {
		&:first-of-type {
			padding-top: 1rem;
		}
		&:last-of-type {
			padding-bottom: 1rem;
		}
	}
`;

const Paragraph = styled.p`
	display: block;
`;

//////////////////////////////////////
// GRAPHQL QUERIES & TYPES ///////////
//////////////////////////////////////

export const GET_LYRICS = gql`
	query getTrackDetails($input: TrackIndicesInput!) {
		track(input: $input) {
			details {
				lyrics
			}
		}
	}
`;

interface Response {
	track: {
		details: {
			lyrics: string;
		};
	};
}

//////////////////////////////////////////////
// REACT COMPONENT ///////////////////////////
//////////////////////////////////////////////

const Lyrics = (props: Props) => {
	const client = useApolloClient();
	const recordContext = useContext(RecordContext);
	const discographyContext = useContext(DiscographyContext);
	const currentTrack = recordContext?.currentTrack;
	const currentRecord = discographyContext?.currentRecord;
	const container = useRef<HTMLDivElement | null>(null);
	const [content, setContent] = useState('');
	const [getLyrics] = useLazyQuery<Response, QueryParams>(GET_LYRICS, {
		onCompleted: (data: Response) => {
			setContent(data.track.details.lyrics);
		},
		onError: error => {
			throw error;
		},
	});

	useEffect(() => {
		if (props.show) {
			if (currentTrack != null && currentRecord != null) {
				const variables = {
					input: {
						track: currentTrack,
						record: currentRecord,
					},
				};
				try {
					const lyrics = client.readQuery<Response, QueryParams>({ query: GET_LYRICS, variables })?.track.details.lyrics;
					if (lyrics) {
						setContent(lyrics);
					}
				} catch (error) {
					getLyrics({
						variables,
					});
				}
			}
		}
	}, [currentRecord, currentTrack, client, getLyrics, props.show]);

	return (
		<CSSTransition timeout={300} in={props.show && !!content} classNames={'fade'} mountOnEnter unmountOnExit>
			<LyricsWrapper ref={container}>
				{React.Children.toArray(
					recordContext?.currentTrack != null &&
						discographyContext?.currentRecord != null &&
						content?.split('\n').map((block: string) => {
							return <Paragraph>{block}</Paragraph>;
						}),
				)}
			</LyricsWrapper>
		</CSSTransition>
	);
};

// EXPORT
export default Lyrics;
