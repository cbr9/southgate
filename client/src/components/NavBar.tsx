import React from 'react';
import styled from 'styled-components';
import { ThemeValues } from '../theme/theme';
import { NavLink } from 'react-router-dom';

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const Wrapper = styled.nav`
	display: block;
	width: 100%;
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	background-color: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	z-index: 10;
	height: ${(props: ThemeValues) => props.theme.navBarHeight}vh;
	text-align: center;
	box-shadow: ${(props: ThemeValues) => props.theme.baseShadow};
`;

const LinkList = styled.ul`
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: space-around;
	& > li {
		list-style: none;
		background-color: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
		transition: all 0.3s;
		a {
			transition: all 0.1s;
		}
		a:link,
		a:visited {
			color: inherit;
			text-transform: uppercase;
			text-decoration: none;
		}
		&:hover {
			a {
				border-bottom: 2px solid white;
				a.active {
					border-bottom: 0 solid white;
				}
			}
		}
		a.active {
			border-bottom: 2px solid white;
		}
	}
`;

//////////////////////////////////////
// REACT COMPONENTS //////////////////
//////////////////////////////////////

function scrollToTop() {
	window.scrollTo(0, 0);
}

const Link = (props: { name: string; path: string }) => {
	return (
		<li>
			<NavLink exact onClick={scrollToTop} to={props.path} rel={props.name}>
				{props.name}
			</NavLink>
		</li>
	);
};

const NavBar = () => {
	return (
		<Wrapper>
			<LinkList>
				<Link path="/bio" name="biografia" />
				<Link path="/novedades" name="novedades" />
				<Link path="/discografia" name="discografia" />
				<Link path="/galeria" name="galeria" />
				<Link path="/contacto" name="contacto" />
			</LinkList>
		</Wrapper>
	);
};

// EXPORT
export default NavBar;
