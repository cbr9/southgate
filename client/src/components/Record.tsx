import React, { useCallback, useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import AudioPlayer from './AudioPlayer';
import Track, { TrackInfo } from './Track';
import { Card, ThemeValues } from '../theme/theme';
import Lyrics from './Lyrics';
import { CSSTransition } from 'react-transition-group';
import { urlFromBackend } from '../index';
import { Context as DiscographyProps, DiscographyContext } from '../containers/Discography/Discography';

//////////////////////////////////////
// INTERFACES ////////////////////////
//////////////////////////////////////

export interface Details {
	id: string;
	image: string;
	title: string;
	year: string;
	tracks: TrackInfo[];
}

interface Props {
	info: Details;
	index: number;
}

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const Wrapper = styled(Card)`
	display: flex;
	width: 95vw;
	height: auto;
	@media only screen and (max-width: 1024px) and (orientation: portrait) {
		flex-direction: column;
		width: min-content;
	}

	&:not(:last-of-type) {
		margin-bottom: 3rem;
	}
`;

const Left = styled.div`
	position: relative;
	flex-basis: 50%;
	@media only screen and (max-width: 1024px) and (orientation: portrait) {
		height: 40vh;
		width: auto;
		flex-basis: unset;
	}
`;

const Tracks = styled.div`
	height: 100%;
	width: 100%;
	text-align: center;
	position: relative;
	padding: 1.5rem 2%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	z-index: 0;
	h2 {
		margin-bottom: 3%;
		text-decoration: underline;
	}
`;

const LyricsBtn = styled.div`
	position: absolute;
	bottom: 0;
	right: 0;
	width: 20%;
	height: 15%;
	box-shadow: ${(props: ThemeValues) => props.theme.baseShadow};
	background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	border: none;
	color: white;
	cursor: pointer;
	font-size: 80%;
	z-index: 20;
	transition: all 0.1s;
	padding: 0 0.5rem;

	&.fade-enter {
		opacity: 0;
	}
	&.fade-enter-active {
		opacity: 1;
		transition: opacity 300ms;
	}
	&.fade-exit {
		opacity: 1;
	}
	&.fade-exit-active {
		opacity: 0;
		transition: opacity 300ms;
	}
`;

const Right = styled.div`
	position: relative;
	//min-width: 40vw;
	flex-basis: 50%;
	height: fit-content;
	@media only screen and (max-width: 1024px) and (orientation: portrait) {
		height: 40vh;
		width: auto;
		flex-basis: unset;
	}
`;

const RecordImage = styled.img`
	box-shadow: ${(props: ThemeValues) => props.theme.baseShadow};
	display: block;
	height: auto;
	width: 100%;
	z-index: -1;
	@media only screen and (max-width: 1024px) and (orientation: portrait) {
		height: 100%;
		width: auto;
		max-width: unset;
	}
`;

///////////////////////////////////////
// REACT COMPONENT ////////////////////
///////////////////////////////////////

export interface Context {
	updateTrack: (index: number) => void;
	currentTrack: number | undefined;
	toggleButtons: () => void;
	blockedBtn: boolean;
}

export const RecordContext = React.createContext<Context | undefined>(undefined);

const Record = (props: Props) => {
	const [showPlayer, setShowPlayer] = useState(false);
	const [showLyrics, setShowLyrics] = useState(false);
	const [blockedBtn, setBlockedBtn] = useState(false);
	const [currentTrack, setCurrentTrack] = useState<number | undefined>(undefined);
	const context = useContext(DiscographyContext);
	const { currentRecord } = context as DiscographyProps;

	useEffect(() => {
		return () => {
			// state changes are batched, so these state changes do not trigger three render cycles
			// clean up
			setShowPlayer(false);
			setShowLyrics(false);
			setCurrentTrack(undefined);
		};
	}, []);

	const toggleButtons = useCallback(() => {
		setBlockedBtn((prevState: boolean) => !prevState);
	}, []);

	const updateTrack = useCallback((index: number) => {
		setCurrentTrack(index);
	}, []);

	useEffect(() => {
		if (currentRecord !== props.index && showPlayer) {
			setShowPlayer(false);
			setShowLyrics(false);
		} else if (currentRecord === props.index && !showPlayer) {
			setShowPlayer(true);
		}
	}, [currentRecord, props.index, showPlayer]);

	return (
		<RecordContext.Provider
			value={{
				blockedBtn,
				currentTrack,
				toggleButtons,
				updateTrack,
			}}
		>
			<Wrapper>
				<Left>
					<Tracks>
						<h2>
							{props.info.title} ({props.info.year})
						</h2>
						<ul>
							{React.Children.toArray(
								props.info.tracks.map((track: TrackInfo, index: number) => {
									return <Track record={props.index} title={track.title} index={index} />;
								}),
							)}
						</ul>
					</Tracks>
					<Lyrics show={showLyrics} />
					<CSSTransition timeout={300} in={showPlayer} classNames={'fade'} mountOnEnter unmountOnExit>
						<LyricsBtn onClick={() => setShowLyrics(!showLyrics)}>{!showLyrics ? 'Letra' : 'Canciones'}</LyricsBtn>
					</CSSTransition>
				</Left>
				<Right>
					{/*TODO: PREFETCH IMAGES*/}
					<RecordImage src={urlFromBackend(props.info.image)} />
					<AudioPlayer show={showPlayer} index={props.index} />
				</Right>
			</Wrapper>
		</RecordContext.Provider>
	);
};

// EXPORT
export default React.memo(Record);
