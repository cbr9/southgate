import React from 'react';
import styled from 'styled-components';
import { ThemeValues } from '../theme/theme';
import { Link } from 'react-router-dom';
import { ReactComponent as Facebook } from '../assets/svg/facebook.svg';
import { ReactComponent as Instagram } from '../assets/svg/instagram.svg';
import { ReactComponent as AppleMusic } from '../assets/svg/applemusic.svg';
import { ReactComponent as Twitter } from '../assets/svg/twitter.svg';
import { ReactComponent as Youtube } from '../assets/svg/youtube.svg';

//////////////////////////////////////
// STYLED COMPONENTS /////////////////
//////////////////////////////////////

const Wrapper = styled.footer`
	margin-top: auto; // don't touch this
	width: 100%;
	padding: 0 2rem 2rem;
	box-shadow: ${(props: ThemeValues) => props.theme.baseShadow};
	background: ${(props: ThemeValues) => props.theme.colorBgIndexDark};
	display: grid;
	grid-template-rows: repeat(auto-fit, min-content);
	grid-template-columns: repeat(5, 1fr);
	grid-template-areas:
		'facebook instagram apple_music twitter youtube'
		'legal    legal     legal       legal   legal';
	grid-column-gap: 0.5rem;
	grid-row-gap: 0.5rem;
`;

const SocialMediaLink = styled.a`
	#facebook {
		grid-area: facebook;
	}
	#instagram {
		grid-area: instagram;
	}
	#apple_music {
		grid-area: apple_music;
	}
	#twitter {
		grid-area: twitter;
	}
	#youtube {
		grid-area: youtube;
	}
	height: 50%;
	width: 25%;
	align-self: center;
	justify-self: center;
	& > svg {
		height: 100%;
		width: 100%;
		& path {
			fill: currentColor;
		}
	}
	@media (max-width: 900px) {
		height: 25%;
		width: 50%;
	}
`;

const LegalInfoWrapper = styled.div`
	grid-area: legal;
	align-self: center;
	justify-self: center;
	font-size: 90%;
	display: flex;
	align-items: center;
	text-align: center;
	border-top: 3px solid white;
	padding-top: 2rem;
	p {
		padding-left: 1rem;
		padding-right: 1rem;
	}
	span {
		display: block;
	}
`;

//////////////////////////////////////
// REACT COMPONENT ///////////////////
//////////////////////////////////////

const Footer: React.FC = () => {
	return (
		<Wrapper>
			<SocialMediaLink
				id="facebook"
				className="outlined"
				href="https://es-es.facebook.com/southgatebandspain/"
				target="_blank"
				rel="noopener noreferrer"
			>
				<Facebook />
			</SocialMediaLink>
			<SocialMediaLink
				id="instagram"
				className="outlined"
				href="https://www.instagram.com/southgateband/"
				target="_blank"
				rel="noopener noreferrer"
			>
				<Instagram />
			</SocialMediaLink>
			<SocialMediaLink
				id="apple_music"
				className="outlined"
				href="https://music.apple.com/ni/album/memories-of-redemption/1228625780"
				target="_blank"
				rel="noopener noreferrer"
			>
				<AppleMusic />
			</SocialMediaLink>
			<SocialMediaLink
				id="twitter"
				className="outlined"
				href="https://twitter.com/Southgate_band?lang=es"
				target="_blank"
				rel="noopener noreferrer"
			>
				<Twitter />
			</SocialMediaLink>
			<SocialMediaLink
				id="youtube"
				className="outlined"
				href="https://www.youtube.com/channel/UC5srBVJgT9FBaLh-ji7_zzw/featured"
				target="_blank"
				rel="noopener noreferrer"
			>
				<Youtube />
			</SocialMediaLink>
			<LegalInfoWrapper>
				<p>
					<span>
						Para contrataciones o merchandising puedes escribirnos a info@southgate.com o utilizar el formulario de
						<Link to="/contact" onClick={() => window.scrollTo(0, 0)}>
							<strong> contacto</strong>
						</Link>
					</span>
					<span>
						Imagen de <a href="https://unsplash.com/@mourner">Vladimir Agafonkin</a> (
						<a href="https://unsplash.com">Unsplash</a>).
					</span>
					<span>&copy;SouthGate es una marca registrada.</span>
					<Link to="/login">Admin</Link>
				</p>
			</LegalInfoWrapper>
		</Wrapper>
	);
};

// EXPORT
export default Footer;
