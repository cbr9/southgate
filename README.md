# SouthGate-Rock-Band

This is a web-app project built for a heavy metal music group.
The frontend is (or will be) built with:
  - TypeScript
  - React
  - CSS-in-JS (styled-components)
  - Next.js, maybe?
  - Apollo Client
  
The backend is built with:
  - Go
  - MongoDB
  - GraphQL
