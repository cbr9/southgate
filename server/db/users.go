package db

import (
	"context"
	"github.com/cbr9/southgate/server/graphql/models"
	"go.mongodb.org/mongo-driver/bson"
)

func (db DB) getUserByField(ctx context.Context, key string, value interface{}) (*models.User, error) {
	var user models.User
	err := db.Users.FindOne(ctx, bson.M{
		key: value,
	}).Decode(&user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (db DB) GetUserByID(ctx context.Context, value string) (*models.User, error) {
	return db.getUserByField(ctx, "_id", value)
}

func (db DB) GetUserByEmail(ctx context.Context, value string) (*models.User, error) {
	return db.getUserByField(ctx, "email", value)
}

func (db DB) GetUserByUsername(ctx context.Context, value string) (*models.User, error) {
	return db.getUserByField(ctx, "username", value)
}
