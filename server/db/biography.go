package db

import (
	"context"
	"github.com/cbr9/southgate/server/graphql/models"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

func (db *DB) GetBio(ctx context.Context) (*models.Biography, error) {
	all := make([]models.Biography, 1)
	cursor, err := db.Biography.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	if err = cursor.All(ctx, &all); err != nil {
		log.Fatal(err)
	}
	return &all[0], err
}

func (db *DB) UpdateBio(ctx context.Context, input *models.NewBiographyInput) (*models.Biography, error) {
	update, err := input.CheckValuesForNil()
	if err != nil {
		return nil, err
	}
	if update != nil {
		_ = db.Biography.FindOneAndUpdate(ctx, bson.D{}, bson.D{
			{"$set", *update},
		})
		var bio models.Biography
		if input.Image != nil {
			bio.Image = (*update)["image"].(string)
		}
		if input.Content != nil {
			bio.Image = (*update)["content"].(string)
		}
		return &bio, nil
	}
	// if both provided values are nil, don't update
	return nil, nil
}
