package db

import (
	"context"
	"github.com/cbr9/southgate/server/graphql/models"
	"github.com/cbr9/southgate/server/uploads"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

func (db *DB) GetMusic(ctx context.Context) (music []*models.Album, err error) {
	cursor, err := db.Music.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	if err = cursor.All(ctx, &music); err != nil {
		log.Fatal(err)
	}
	return music, err
}

func (db *DB) NewAlbum(ctx context.Context, input *models.NewAlbumInput) (models.Album, error) {
	// TODO SHOULD BE IMPLEMENTED AS AN UPDATE THAT, IF DOESNT FIND MATCHING ELEMENTS, INSERTS ONE
	panic("implement me!")
}

func (db *DB) NewTrack(input *models.NewTrackInput) (models.Track, error) {
	if input.Details == nil {
		return models.Track{}, nil
	}

	filename, err := uploads.SaveFileIfNotExist(&input.Details.Audio)
	if err != nil {
		return models.Track{}, nil
	}
	return models.Track{
		Title: input.Title,
		Details: models.TrackDetails{
			Lyrics: input.Details.Lyrics,
			Audio:  filename,
		},
	}, nil
}
