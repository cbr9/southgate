package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"time"
)

type DB struct {
	Music     *mongo.Collection
	Biography *mongo.Collection
	Users     *mongo.Collection
	Messages  *mongo.Collection
}

func ConnectToDB() DB {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("DB")))
	if err != nil {
		log.Fatalf("Error when connecting to DB: %v", err)
	}
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatalf("Error when connecting to DB: %v", err)
	}
	db := client.Database("southgate")

	return DB{
		Music:     db.Collection("music"),
		Biography: db.Collection("biography"),
		Users:     db.Collection("users"),
		Messages:  db.Collection("messages"),
	}
}
