package db

import (
	"context"
	"fmt"
	"github.com/cbr9/southgate/server/graphql/models"
	"log"
)

func (db *DB) InsertMessage(ctx context.Context, message *models.Message) error {
	result, err := db.Messages.InsertOne(ctx, message)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Inserted a message: ", result.InsertedID)
	return err
}
