package router

import (
	"github.com/99designs/gqlgen/handler"
	"github.com/cbr9/southgate/server/db"
	"github.com/cbr9/southgate/server/graphql"
	"github.com/cbr9/southgate/server/graphql/resolvers"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strings"
)

func (r Router) handleGraphQL(db *db.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		h := handler.GraphQL(graphql.NewExecutableSchema(graphql.Config{
			Resolvers: &resolvers.Resolver{
				DB: db,
			},
		}))
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func (r Router) playgroundHandler() gin.HandlerFunc {
	h := handler.Playground("GraphQL", "/query")
	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func (r Router) handleIndex() gin.HandlerFunc {
	return func(c *gin.Context) {
		if strings.HasSuffix(c.Request.URL.Path, "/admin") {
			token, err := parseToken(c.Request)
			if err != nil {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}
			if !token.Valid {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}
		}
		if pathContainsAny(c.Request.URL.Path, r.PublicPaths...) {
			c.Writer.WriteHeader(http.StatusOK)
		}
		err := r.Template.Execute(c.Writer, nil)
		if err != nil {
			log.Println(err)
		}
	}
}

func pathContainsAny(path string, substrings ...string) bool {
	for _, substring := range substrings {
		if strings.HasSuffix(path, substring) {
			return true
		}
	}
	return false
}