package router

import (
	"context"
	"github.com/cbr9/southgate/server/db"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/unrolled/secure"
)

func (r Router) cache() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Cache-Control", "max-age=518400")
	}
}

func (r Router) redirectToHTTPS() gin.HandlerFunc {
	return func(c *gin.Context) {
		secureMiddleware := secure.New(secure.Options{
			SSLRedirect: true,
			SSLHost:     "localhost:443",
		})
		err := secureMiddleware.Process(c.Writer, c.Request)
		if err != nil {
			return
		}
		c.Next()
	}
}

func (r Router) ginContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), "GinContextKey", c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

func (r Router) auth(db *db.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer c.Next()
		token, err := parseToken(c.Request)
		if err != nil {
			return
		}
		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			return
		}
		user, err := db.GetUserByID(c, claims["jti"].(string))
		if err != nil {
			return
		}
		ctx := context.WithValue(c.Request.Context(), CurrentUserKey, user)
		c.Request = c.Request.WithContext(ctx)
	}
}
