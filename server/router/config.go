package router

import (
	"github.com/cbr9/southgate/server/db"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func (r Router) ConfigureRouter(db *db.DB) {
	r.setMiddleware(db)
	r.setRoutes(db)
}

func (r Router) setMiddleware(db *db.DB) {
	r.Use(r.auth(db))
	r.Use(r.redirectToHTTPS())
	r.Use(r.ginContextToContextMiddleware())
	r.Use(r.setCors())
}

func (r Router) setCors() gin.HandlerFunc {
	return cors.New(cors.Config{
		AllowAllOrigins: false,
		AllowOrigins: []string{
			"https://localhost",
			"https://localhost:443",
			"http://localhost:3000",
		},
		AllowMethods:     []string{"GET", "POST"},
		AllowHeaders:     []string{"X-Requested-With", "Content-Type", "Authorization"},
		AllowCredentials: true,
	})
}

func (r Router) setRoutes(db *db.DB) {
	r.Use(r.cache()).Static("/static/media", "./static/media")
	r.POST(endpoint, r.handleGraphQL(db))
	//r.NoRoute(r.handleIndex())
	//r.Use(r.cache()).Static("/", "../client/build")
	r.GET("/", r.playgroundHandler())
}
