package router

import (
	"github.com/gin-gonic/gin"
	"html/template"
	"log"
)

const (
	endpoint = "/query"
)

type Router struct {
	*gin.Engine
	*template.Template
	PublicPaths []string
}

func NewRouter() Router {
	index, err := template.ParseFiles("../client/build/index.html")
	if err != nil {
		log.Fatal(err)
	}
	return Router{
		Engine:      gin.Default(),
		Template:    index,
		PublicPaths: []string{"/discography", "/gallery", "/login", "/bio", "/contact"}}
}
