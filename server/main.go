package main

import (
	"errors"
	"github.com/cbr9/southgate/server/db"
	"github.com/cbr9/southgate/server/graphql/resolvers"
	"github.com/cbr9/southgate/server/router"
	"github.com/joho/godotenv"
	"log"
	"os"
)

const (
	defaultPort = "8080"
	securePort  = "443"
)

type server struct {
	router router.Router
	db     db.DB
}

func newServer() *server {
	s := &server{
		router: router.NewRouter(),
		db:     db.ConnectToDB(),
	}
	s.router.ConfigureRouter(&s.db)
	return s
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalln(err)
	}

	// check to avoid possible errors when making changes. Import cycles are nasty
	//noinspection GoBoolExpressions
	if router.CurrentUserKey == resolvers.CurrentUserKey {
		server := newServer()
		go server.router.Run(":" + os.Getenv("API_PORT"))
		log.Fatal(server.router.RunTLS(":"+os.Getenv("SECURE_API_PORT"), "localhost.crt", "localhost.key"))
	} else {
		log.Println(errors.New("CurrentUserKey constant from router and resolvers pkg are different"))
	}
}
