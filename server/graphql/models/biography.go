package models

import (
	"github.com/99designs/gqlgen/graphql"
	"github.com/cbr9/southgate/server/uploads"
	"go.mongodb.org/mongo-driver/bson"
)

type Biography struct {
	Image   string `json:"image" bson:"image"`
	Content string `json:"content" bson:"content"`
}

type NewBiographyInput struct {
	Image   *graphql.Upload `json:"image" bson:"image"`
	Content *string         `json:"content" bson:"content"`
}

func (b NewBiographyInput) CheckValuesForNil() (*bson.M, error) {
	if b.Image == nil && b.Content == nil {
		return nil, nil
	}
	update := make(bson.M, 2)
	var bio Biography
	if b.Image != nil {
		filename, err := uploads.SaveFileIfNotExist(b.Image)
		if err != nil {
			return nil, err
		}
		update["image"] = filename
		bio.Image = filename
	}
	if b.Content != nil {
		update["content"] = *b.Content
		bio.Content = *b.Content
	}

	return &update, nil
}

//func (b NewBiographyInput) GetFiles() []*graphql.Upload {
//	var fields map[string]interface{}
//	var files []*graphql.Upload
//	bytes, _ := json.Marshal(b)
//	err := json.Unmarshal(bytes, &fields)
//	if err != nil {
//		log.Fatal(err)
//		return nil
//	}
//	for _, field := range fields {
//		fmt.Println(field)
//		switch field.(type) {
//		case *graphql.Upload:
//			files = append(files, field.(*graphql.Upload))
//			file := graphql.Upload{
//				File:     ,
//				Filename: "",
//				Size:     0,
//			}
//			break
//		default:
//			continue
//		}
//	}
//	fmt.Println(files)
//	return nil
//}
