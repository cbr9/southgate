package models

import "github.com/99designs/gqlgen/graphql"

type Album struct {
	ID     string  `json:"_id" bson:"_id"`
	Image  string  `json:"image" bson:"image"`
	Title  string  `json:"title" bson:"title"`
	Year   int     `json:"year" bson:"year"`
	Tracks []Track `json:"tracks" bson:"tracks"`
}

type NewAlbumInput struct {
	Image  graphql.Upload   `json:"image"`
	Year   int              `json:"year"`
	Title  string           `json:"title"`
	Tracks []*NewTrackInput `json:"tracks"`
}

type Track struct {
	Title   string `json:"title" bson:"title"`
	Details TrackDetails
}

type NewTrackInput struct {
	Title   string                `json:"title"`
	Details *NewTrackDetailsInput `json:"details"`
}

type TrackDetails struct {
	Lyrics string `json:"lyrics" bson:"lyrics"`
	Audio  string `json:"audio" bson:"audio"`
}

type NewTrackDetailsInput struct {
	Audio  graphql.Upload `json:"audio"`
	Lyrics string         `json:"lyrics"`
}

type TrackIndicesInput struct {
	Track  int `json:"track"`
	Record int `json:"record"`
}
