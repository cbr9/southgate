package models

type Message struct {
	Name  string `json:"name" bson:"name"`
	Email string `json:"email" bson:"email"`
	Body  string `json:"body" bson:"body"`
}

type MessageInput struct {
	Body  string `json:"body"`
	Email string `json:"email"`
	Name  string `json:"name"`
}
