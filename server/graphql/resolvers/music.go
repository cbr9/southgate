package resolvers

import (
	"context"
	"errors"
	"github.com/cbr9/southgate/server/graphql/models"
	"log"
)

func (r *queryResolver) NumberRecords(ctx context.Context) (int, error) {

	music, err := r.DB.GetMusic(ctx)
	if err != nil {
		return -1, err
	}
	return len(music), nil
}

func (r *queryResolver) Track(ctx context.Context, input models.TrackIndicesInput) (*models.Track, error) {
	music, err := r.DB.GetMusic(ctx)
	if err != nil {
		log.Fatal(err)
	}
	if input.Record >= len(music) {
		return nil, errors.New("record out of range")
	} else if input.Track > len(music[input.Record].Tracks) {
		return nil, errors.New("track out of range")
	}
	return &music[input.Record].Tracks[input.Track], err
}

func (r *queryResolver) Records(ctx context.Context) ([]*models.Album, error) {
	return r.DB.GetMusic(ctx)
}

func (m *mutationResolver) AddAlbum(ctx context.Context, input models.NewAlbumInput) (*models.Album, error) {
	// NEED AUTHORIZATION
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		return nil, err
	}
	_, err = GetCurrentUserFromContext(gc)
	if err != nil {
		return nil, ErrUnauthenticated
	}
	panic("implement me")
}
