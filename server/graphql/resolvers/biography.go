package resolvers

import (
	"context"
	"github.com/cbr9/southgate/server/graphql/models"
)

func (r *queryResolver) Biography(ctx context.Context) (*models.Biography, error) {
	return r.DB.GetBio(ctx)
}

func (m *mutationResolver) NewBio(ctx context.Context, input models.NewBiographyInput) (*models.Biography, error) {
	err := RequireAuth(ctx)
	if err != nil {
		return nil, err
	}
	// TODO SHOULD TAKE THE CURRENT BIO AS INPUT
	return m.UpdateBio(ctx, &input)
}
