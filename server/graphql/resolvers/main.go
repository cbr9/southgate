//go:generate go run github.com/99designs/gqlgen -v

package resolvers

import (
	"context"
	"fmt"
	"github.com/cbr9/southgate/server/db"
	"github.com/cbr9/southgate/server/graphql"
	"github.com/cbr9/southgate/server/graphql/models"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

const CurrentUserKey = "currentUser"

var ErrUnauthenticated = errors.New("unauthenticated")

type Resolver struct{ *db.DB }
type queryResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }

func (r *Resolver) Query() graphql.QueryResolver {
	return &queryResolver{r}
}

func (r *Resolver) Mutation() graphql.MutationResolver {
	return &mutationResolver{r}
}

func GetCurrentUserFromContext(c *gin.Context) (*models.User, error) {
	errNoUserInContext := errors.New("no user in context")
	if c.Request.Context().Value(CurrentUserKey) == nil {
		return nil, errNoUserInContext
	}
	user, ok := c.Request.Context().Value(CurrentUserKey).(*models.User)
	if !ok || user.ID == "" {
		return nil, errNoUserInContext
	}
	return user, nil
}

func GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value("GinContextKey")
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}

func RequireAuth(ctx context.Context) error {
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		return err
	}
	_, err = GetCurrentUserFromContext(gc)
	if err != nil {
		return ErrUnauthenticated
	}
	return nil
}
