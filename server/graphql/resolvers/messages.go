package resolvers

import (
	"context"
	"github.com/cbr9/southgate/server/graphql/models"
)

func (m *mutationResolver) SendMessage(ctx context.Context, data models.MessageInput) (*models.Message, error) {
	message := models.Message{
		Name:  data.Name,
		Email: data.Email,
		Body:  data.Body,
	}
	err := m.DB.InsertMessage(ctx, &message)
	return &message, err
}
