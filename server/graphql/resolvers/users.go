package resolvers

import (
	"context"
	"errors"
	"github.com/cbr9/southgate/server/graphql/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
)

var (
	ErrBadCredentials = errors.New("wrong email-password combination")
)

func (m *mutationResolver) Login(ctx context.Context, input models.LoginInput) (*models.AuthResponse, error) {
	user, err := m.GetUserByEmail(ctx, input.Email)
	if err != nil {
		return nil, ErrBadCredentials
	}
	err = user.ComparePassword(input.Password)
	if err != nil {
		return nil, ErrBadCredentials
	}
	token, err := user.GenToken()
	if err != nil {
		return nil, errors.New("something went wrong")
	}
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		log.Println(err)
	}
	gc.SetCookie("token", token.AccessToken, 60*60*2, "/admin", "", true, true)
	return &models.AuthResponse{
		AuthToken: token,
		User:      user,
	}, nil
}

func (m *mutationResolver) Register(ctx context.Context, input models.RegisterInput) (*models.AuthResponse, error) {
	_, err := m.GetUserByEmail(ctx, input.Email)
	if err == nil {
		// if it was found, return an error
		return nil, errors.New("email already in use")
	}
	_, err = m.GetUserByUsername(ctx, input.Username)
	if err == nil {
		// if it was found, return an error
		return nil, errors.New("username already in use")
	}
	user := &models.User{
		Name:      input.Username,
		Email:     input.Email,
		FirstName: input.FirstName,
		LastName:  input.LastName,
	}
	err = user.HashPassword(input.Password)
	if err != nil {
		log.Printf("error while hashing password: %v", err)
		return nil, errors.New("something went wrong")
	}
	user.ID = primitive.NewObjectID().String()
	_, err = m.DB.Users.InsertOne(ctx, user)
	if err != nil {
		log.Printf("error creating a user: %v", user)
		return nil, err
	}
	token, err := user.GenToken()
	if err != nil {
		log.Printf("error while generating token")
		return nil, errors.New("something went wrong")
	}
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		log.Println(err)
	}
	gc.SetCookie("token", token.AccessToken, 60*60*2, "/admin", "", true, true)
	return &models.AuthResponse{
		AuthToken: token,
		User:      user,
	}, nil
}
