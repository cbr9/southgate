module github.com/cbr9/southgate/server

go 1.13

require (
	github.com/99designs/gqlgen v0.10.2
	github.com/agnivade/levenshtein v1.0.3 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/contrib v0.0.0-20191209060500-d6e26eeaa607
	github.com/gin-gonic/gin v1.5.0
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/unrolled/secure v1.0.7
	github.com/vektah/gqlparser v1.3.1
	go.mongodb.org/mongo-driver v1.3.0
	golang.org/x/crypto v0.0.0-20200210222208-86ce3cb69678
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
