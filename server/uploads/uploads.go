package uploads

import (
	"github.com/99designs/gqlgen/graphql"
	"go.mongodb.org/mongo-driver/bson"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Upload interface {
	CheckValuesForNil() (*bson.M, error)
}

func SaveFileIfNotExist(f *graphql.Upload) (string, error) {
	f.Filename = filepath.Join("./static/media", f.Filename)
	_, err := os.Stat(f.Filename)
	if err != nil {
		err = SaveUploadedFile(f)
		if err != nil {
			return "", err
		}
	}
	return "/" + f.Filename, nil
}

func SaveUploadedFile(f *graphql.Upload) error {
	file, err := os.Create(f.Filename)
	if err != nil {
		return err
	}
	content, err := ioutil.ReadAll(f.File)
	if err != nil {
		return err
	}
	_, err = file.Write(content)
	if err != nil {
		return err
	}
	return nil
}
